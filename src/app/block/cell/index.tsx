import React from 'react'
import { View, StyleSheet } from 'react-native'

interface Props {
	x: number,
	color: string
}

const CellComponent = ({ x, color }: Props) => (
	<View
		style={[ styles.cell, {
			left: x * 17,
			backgroundColor: color
		} ]}
	/>
)

const styles = StyleSheet.create({
	cell: {
		position: 'absolute',
		width: 17,
		height: 17,
	}
})

export default CellComponent
