import React from 'react'
import { View } from 'react-native'
import { observer } from 'mobx-react'

import { Cell } from '../../world/model'
import RowComponent from './row'

interface Props {
	x: number
	y: number
	cellss: Cell[][]
}

@observer
class BlockComponent extends React.Component<Props, object> {
	render () {
		const { x, y, cellss } = this.props

		return (
			<View
				style={{
					position: 'absolute',
					top: y * 17,
					left: x * 17,
				}}
			>
				{cellss.map((cells: Cell[], i: number) => (
					<RowComponent
						key={i}
						cells={cells}
					/>
				))}
			</View>
		)
	}
}

export default BlockComponent