import React from 'react'
import { View } from 'react-native'
import { observer } from 'mobx-react'

import { Cell } from '../../../world/model'
import CellComponent from '../cell'

@observer
class BlockRow extends React.Component<{ cells: Cell[] }, object> {
	render () {
		const { cells } = this.props

		return (
			<View
				style={{
					position: 'relative',
					height: 17,
				}}
			>
				{cells.map((cell, i) => cell && (
					<CellComponent
						key={i}
						x={i}
						color={cell.color}
					/>
				))}
			</View>
		)
	}
}

export default BlockRow
