import React from 'react'
import { View, TouchableOpacity } from 'react-native'

import GameStore from '../../world/game.store'

interface Props {
	gameStore: GameStore
}

class ControllerComponent extends React.Component<Props, object> {

	intervalId: number

	_onPressInLeft = () => {
		this.intervalId = setInterval(
			this.props.gameStore.left,
			50
		)
	}

	_onPressOutLeft = () => {
		clearTimeout(this.intervalId)
	}

	_onPressInRight = () => {
		this.intervalId = setInterval(
			this.props.gameStore.right,
			50
		)
	}

	_onPressOutRight = () => {
		clearTimeout(this.intervalId)
	}

	_onPressInDown = () => {
		this.intervalId = setInterval(
			this.props.gameStore.tick,
			50
		)
	}

	_onPressOutDown = () => {
		clearTimeout(this.intervalId)
	}

	render () {
		const { gameStore } = this.props

		return (
			<View
				style={{
					alignItems: 'center',
				}}>
				<TouchableOpacity onPress={gameStore.rotate}>
					<View
						style={{
							width: 50,
							height: 50,
							backgroundColor: '#aaa'
						}}
					>
					</View>
				</TouchableOpacity>
				<View
					style={{
						flexDirection: "row"
					}}
				>
					<TouchableOpacity
						onPress={gameStore.left}
						onPressIn={this._onPressInLeft}
						onPressOut={this._onPressOutLeft}
					>
						<View
							style={{
								width: 50,
								height: 50,
								backgroundColor: '#aaa'
							}}
						>
						</View>
					</TouchableOpacity>
					<TouchableOpacity onPress={gameStore.down}>
						<View
							style={{
								width: 50,
								height: 50,
								backgroundColor: '#aaa'
							}}
						>
						</View>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={gameStore.right}
						onPressIn={this._onPressInRight}
						onPressOut={this._onPressOutRight}
					>
						<View
							style={{
								width: 50,
								height: 50,
								backgroundColor: '#aaa'
							}}
						>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		)
	}
}

export default ControllerComponent