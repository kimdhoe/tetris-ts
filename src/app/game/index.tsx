import * as React from 'react'
import { View, StyleSheet } from 'react-native'
import { observer } from 'mobx-react'

import { Block } from '../../world/model'
import BlockComponent from '../block'

interface Props {
	block: Block
	pile: Block
}

@observer
class GameComponent extends React.Component<Props, object> {

	render () {
		const { block, pile } = this.props

		return (
			<View style={styles.scene}>
				{block && (
					<BlockComponent
						x={block.x}
						y={block.y}
						cellss={block.cellss}
					/>
				)}

				<BlockComponent
					x={pile.x}
					y={pile.y}
					cellss={pile.cellss}
				/>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	scene: {
		position: 'relative',
		backgroundColor: '#efefef',
		width: 255,
		height: 476,
		overflow: 'hidden',
	},
})

export default GameComponent
