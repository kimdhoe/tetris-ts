import * as React from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import { inject, observer } from 'mobx-react'

import { Status } from '../world/model'
import GameComponent from './game'
import ControllerComponent from './controller'
import GameStore from '../world/game.store'

interface Props {
	gameStore?: GameStore
}

@inject('gameStore')
@observer
class Game extends React.Component<Props, object> {

	_start = () => {
		this.props.gameStore.startGame()
	}

	render () {
		const { gameStore } = this.props

		return (
			<View style={styles.container}>
				<GameComponent
					block={gameStore.block}
					pile={gameStore.pile}
				/>

				{gameStore.status === Status.Playing && (
					<ControllerComponent gameStore={gameStore} />
				)}

				{gameStore.status === Status.NotPlaying && (
					<View style={styles.info}>
						<TouchableOpacity
							style={styles.button}
							onPress={this._start}
						>
							<Text>START</Text>
						</TouchableOpacity>
					</View>
				)}

				{gameStore.status === Status.GameOver && (
					<View style={styles.info}>
						<TouchableOpacity
							style={styles.button}
							onPress={this._start}
						>
							<Text>GAME OVER</Text>
							<Text>PLAY AGAIN</Text>
						</TouchableOpacity>
					</View>
				)}
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		position: 'relative',
		flex: 1,
		backgroundColor: '#ffcc33'
	},
	scene: {
		position: 'relative',
		backgroundColor: '#efefef',
		width: 255,
		height: 476,
		overflow: 'hidden',
	},
	info: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 250,
		alignItems: 'center',
		justifyContent: 'center',
	},
	button: {
		width: 100,
		height: 50,
		backgroundColor: 'skyblue',
		alignItems: 'center',
		justifyContent: 'center',
	}
})

export default Game
