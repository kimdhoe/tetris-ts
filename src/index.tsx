import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'mobx-react'

import Game from './app'
import GameStore from './world/game.store'
import GameService from './world/game.service'

const gameService = new GameService()
const gameStore = new GameStore(gameService)

function App () {
	return (
		<Provider gameStore={gameStore}>
			<Game />
		</Provider>
	)
}

AppRegistry.registerComponent('tetris', () => App);
