import random from 'lodash.random'

import {
	Block,
	Cell,
	makeCell,
	makeCellss1,
	makeCellss2,
	makeCellss3,
	makeCellss4,
	makeCellss5,
	makeCellss6,
	makeCellss7,
} from './model'

const INITIAL_X = 6
const INITIAL_Y = 0
const BLUE_CELL = makeCell('blue')
const INITIAL_PILE = {
	x: 0,
	y: 0,
	cellss: [
		[], [], [], [], [], [], [], [], [], [], [], [], [], [],
		[], [], [], [], [], [], [], [], [], [], [], [], [], [],
		[
			BLUE_CELL, BLUE_CELL, BLUE_CELL, BLUE_CELL, BLUE_CELL,
			BLUE_CELL, BLUE_CELL, BLUE_CELL, BLUE_CELL, BLUE_CELL,
			BLUE_CELL, BLUE_CELL, BLUE_CELL, BLUE_CELL, BLUE_CELL,
		]
	],
}

class GameService {

	initialPile = INITIAL_PILE

	randomBlock = (): Block => ({
		x: INITIAL_X,
		y: INITIAL_Y,
		cellss: this.randomCellss(),
	})

	randomCellss = (): Cell[][] => {
		const fs = [
			makeCellss1, makeCellss2, makeCellss3, makeCellss4,
			makeCellss5, makeCellss6, makeCellss7,
		]
		const color = this.randomColor()
		const f = fs[random(0, fs.length - 1)]

		return f(color)
	}

	randomColor = (): string => {
		const colors = [
			'red', 'green', 'blue', 'orange', 'skyblue', 'purple',
			'grey', 'pink', 'tomato', 'darkgreen',
		]

		return colors[random(0, colors.length - 1)]
	}

	// Given a pile, determines whether a game is over.
	isOver = (pile: Block): boolean => pile.cellss[0].some(Boolean)

	// Determines whether a block collides with a pile.
	collides = (block: Block, pile: Block): boolean => {
		return block.cellss.some((cells, i) => {
			return cells.some((cell, j) => {
				return !!(
					cell &&
					pile.cellss.length > block.y + i &&
					pile.cellss[block.y + i] &&
					pile.cellss[block.y + i].length > block.x + j &&
					pile.cellss[block.y + i][block.x + j]
				)
			})
		})
	}

	// Given a list of lists of xs, produces a rotated version.
	rotate = <T>(xss: T[][]): T[][] => {
		return xss[0].map(
			(x, j) => xss.map(
				(xs, i) => xss[i].reverse()[j]
			)
		)
	}

	// Merges block into pile.
	// Effect - mutates pile.
	merge = (block: Block, pile: Block): void => {
		block.cellss.forEach(
			// Merges an i-th row in block into the given pile.
			(cells, i) => {
				cells.forEach(
					// Merges a j-th cell in a row into the given pile.
					(cell, j) => {
						if (cell) {
							const x1 = block.x + j
							const y1 = block.y + i
							const rowLength = pile.cellss[y1].length

							if (x1 >= rowLength) {
								const t = x1 - rowLength

								for (let u = 0; u < t; u++) {
									pile.cellss[y1].push(null)
								}
							}

							pile.cellss[y1][x1] = { color: cell.color }
						}
					}
				)
			}
		)
	}

	removeRow = (pile: Block) => {
		for (let i = pile.cellss.length - 2; i >= 0; i--) {
			const row = pile.cellss[i]

			if (row.length === 0) break

			if (row.length === 15 && row.every(Boolean)) {
				pile.cellss.splice(i, 1)
				pile.cellss.unshift([])
			}
		}
	}
}

export default GameService
