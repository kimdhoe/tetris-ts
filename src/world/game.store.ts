import { observable, action, computed } from 'mobx'

import { Block, Status } from './model'
import GameService from './game.service'

class GameStore {

	private game: GameService
	private intervalId: number

	@observable status: Status = Status.NotPlaying
	@observable block: Block = null
	@observable pile: Block

	constructor (game: GameService) {
		this.game = game
		// this.block = game.randomBlock()
		this.pile = game.initialPile
	}

	@action
	tick = () => {
		if (this.game.isOver(this.pile)) {
			this.uninstallTick()
			this.status = Status.GameOver

			return
		}

		const nextBlock = { ...this.block, y: this.block.y + 1 }

		if (this.game.collides(nextBlock, this.pile)) {
			this.game.merge(this.block, this.pile)
			this.game.removeRow(this.pile)
			this.renewBlock()

			return
		}

		this.block.y += 1
	}

	@action
	rotate = () => {
		if (this.block.cellss.length === this.block.cellss[0].length) {
			return
		}

		const isPortrait = this.block.cellss.length > this.block.cellss[0].length
		const rotated = {
			cellss: this.game.rotate(this.block.cellss),
			x: isPortrait ? this.block.x - 1 : this.block.x + 1,
			y: isPortrait ? this.block.y + 1 : this.block.y - 1,
		}

		if (rotated.x < 0) {
			rotated.x = 0
		} else if (rotated.x > 15 - rotated.cellss[0].length) {
			rotated.x = 15 - rotated.cellss[0].length
		}

		if (!this.game.collides(rotated, this.pile)) {
			this.block = rotated
		}
	}

	@action
	left = () => {
		const { block, pile } = this

		if (
			block.x > 0 &&
			!this.game.collides({ ...block, x: block.x - 1 }, pile)
		) {
			block.x -= 1
		}
	}

	@action
	right = () => {
		const { block, pile } = this

		if (
			block.x < 15 - block.cellss[0].length &&
			!this.game.collides({ ...block, x: block.x + 1 }, pile)
		) {
			block.x += 1
		}
	}

	@action
	down = () => {
		let y = this.block.y

		while (!this.game.collides({ ...this.block, y: y + 1 }, this.pile)) {
			y += 1
		}

		this.block.y = y
	}

	@action
	renewBlock = () => {
		this.block = this.game.randomBlock()
	}

	@action
	startGame = () => {
		this.status = Status.Playing
		this.pile = this.game.initialPile
		this.renewBlock()
		this.installTick()
	}

	installTick (interval = 700): void {
		this.intervalId = setInterval(this.tick, interval)
	}

	uninstallTick (): void {
		clearInterval(this.intervalId)
	}
}

export default GameStore
