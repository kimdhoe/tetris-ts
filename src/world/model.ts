interface Block {
	x: number
	y: number
	cellss: (Cell | null)[][]
}

interface Cell {
	color: string
}

enum Status {
	Playing,
	NotPlaying,
	GameOver,
}

const makeCell = (color: string): Cell => ({ color })

const makeCellss1 = (color: string): Cell[][] => [
	[ makeCell(color), null,            null            ],
	[ makeCell(color), makeCell(color), makeCell(color) ],
]

const makeCellss2 = (color: string): Cell[][] => [
	[ makeCell(color), makeCell(color) ],
	[ makeCell(color), makeCell(color) ],
]

const makeCellss3 = (color: string): Cell[][] => [
	[ null,            null,            makeCell(color) ],
	[ makeCell(color), makeCell(color), makeCell(color) ],
]

const makeCellss4 = (color: string): Cell[][] => [
	[ makeCell(color), makeCell(color), null            ],
	[ null,            makeCell(color), makeCell(color) ],
]

const makeCellss5 = (color: string): Cell[][] => [
	[ null,            makeCell(color), makeCell(color) ],
	[ makeCell(color), makeCell(color), null            ],
]

const makeCellss6 = (color: string): Cell[][] => [
	[ makeCell(color) ],
	[ makeCell(color) ],
	[ makeCell(color) ],
	[ makeCell(color) ],
]

const makeCellss7 = (color: string): Cell[][] => [
	[ null,            makeCell(color), null            ],
	[ makeCell(color), makeCell(color), makeCell(color) ],
]

export {
	Block,
	Cell,
	Status,
	makeCell,
	makeCellss1,
	makeCellss2,
	makeCellss3,
	makeCellss4,
	makeCellss5,
	makeCellss6,
	makeCellss7,
}
